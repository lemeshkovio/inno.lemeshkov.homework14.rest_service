package inno.lemeshkov;

import java.util.HashMap;
import java.util.Map;

public class EnvVar {

    private Map<String, String> envVars;

    /**
     * get all environment variables
     */
    public EnvVar() {
        envVars = new HashMap<>(System.getenv());
    }

    public Map<String, String> getEnvVars() {
        return envVars;
    }
}
