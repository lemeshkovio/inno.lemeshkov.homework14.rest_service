package inno.lemeshkov;

import java.util.HashMap;

public class LoneEnvVar {
    private String loneEnvVar;

    /**
     * get environment variable by name
     * get "No such environment variable" if name not represented in environment
     * @param varName - name to get environment variable
     *
     *
     */
    public LoneEnvVar(String varName) {
        String[] name = varName.split(",");
        this.loneEnvVar = System.getenv(name[name.length-1]);
        if (loneEnvVar==null){
            loneEnvVar = "No such environment variable";
        }
    }

    public String getLoneEnvVar() {
        return loneEnvVar;
    }
}
