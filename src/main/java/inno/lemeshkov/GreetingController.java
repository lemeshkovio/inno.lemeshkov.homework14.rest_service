package inno.lemeshkov;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class GreetingController {

    @GetMapping("/")
    public EnvVar getEnvVar() {
        return new EnvVar();
    }

    @GetMapping("/byName")
    public LoneEnvVar getEnvVar(@RequestParam(value = "name", defaultValue = "USER") String name) {
        return new LoneEnvVar(name);
    }
}
